package fr.colyn.tp3.utilities;

import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class Tools {

	private Scanner scanner;

	public Tools() {
		scanner = new Scanner(System.in);
	}

	private static Tools INSTANCE = null;

	public static Tools getInstance() {
		if (INSTANCE == null) {
			synchronized (Tools.class) {
				if (INSTANCE == null) {
					INSTANCE = new Tools();
				}
			}
		}
		return INSTANCE;
	}

	public Integer selectInt(final String msg, final Object[] choices, final Integer min, final Integer max) {

		Integer result = null;
		do {
			System.out.println(msg);
			int looper = 1;
			for (Object choice : choices) {
				System.out.println(looper + " - " + choice.toString());
				looper++;
			}
			try {
				result = this.scanner.nextInt();
			} catch (InputMismatchException e) {
				System.err.println(e.getMessage());
			}

		} while (result < min + 1 || result > max + 1);

		return result - 1;
	}

	public String selectString(final String msg) {

		String result = null;
		do {
			System.out.println(msg);
			try {
				result = this.scanner.nextLine();
			} catch (InputMismatchException e) {
				System.err.println(e.getMessage());
			}

		} while (!result.equals(""));

		return result;
	}

	public String inputString(String msg) {
		System.out.println(msg);
		if (this.scanner.hasNextLine()) {
			this.scanner.nextLine();
		}

		return this.scanner.nextLine();
	}

}
