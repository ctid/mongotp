package fr.colyn.tp3.mongoUser;

import java.util.ArrayList;
import java.util.List;

import org.bson.Document;

import com.mongodb.client.FindIterable;

import fr.colyn.tp3.mongo.MongoService;
import fr.colyn.tp3.utilities.Tools;

public class MongoUserManager {

	private MongoService mongoService;
	
	public MongoUserManager(MongoService mongoService) {
		this.mongoService = mongoService;
	}
	
	//Permet de d�marer les interactions
	public void run() {
		this.askChooseCollection();
	}
	
	//Permet au user de selectionner la collection parmis toutes les collections
	private void askChooseCollection() {
		List<String> lstCollections = this.mongoService.getListCollection();
		
		int input = Tools.getInstance().selectInt("\nQuelle collection voulez-vous utiliser ?", lstCollections.toArray(), 0, lstCollections.size() - 1);
		
		this.askWhatToDo(lstCollections.get(input));
	}
	
	
	//Premet de savoir si le user veut Lire, ecrire ou changer collection
	private void askWhatToDo(String collection) {
		
		List<String> lstChoix = new ArrayList<String>();
		lstChoix.add("Lire");
		lstChoix.add("Ecrire");
		lstChoix.add("Changer de collection");
		int input = Tools.getInstance().selectInt("\nQue voulez-vous faire ?", lstChoix.toArray(), 0, lstChoix.size() - 1);
		
		switch (input) {
		case 0:
			this.readCollection(collection);
			break;
		case 1:
			this.writeInCollection(collection);
			break;
		case 2:
			this.askChooseCollection();
			break;

		default:
			break;
		}
		
		
	}
	
	//Premet de lire une collection selon des crit�res
	private void readCollection(String collection) {
		
		//selection d'un champs parmis tous
		List<String> lstFields = this.mongoService.getCollectionFields(collection);
		int fieldIndex = Tools.getInstance().selectInt("\nSur quel champs voulez-vous filtrer ?", lstFields.toArray(), 0, lstFields.size() - 1);
		
		//ask operator --> selection d'un op�rateur de comparaison
		List<String> lstOperators = new ArrayList<String>();
		lstOperators.add("eq");
		lstOperators.add("ne");
		lstOperators.add("gt");
		lstOperators.add("gte");
		lstOperators.add("lt");
		lstOperators.add("lte");
		lstOperators.add("aucun");
		int operatorIndex = Tools.getInstance().selectInt("\nQuel op�rateur voulez-vous utiliser ?", lstOperators.toArray(), 0, lstOperators.size() - 1);
		
		//ask value --> saisie de la valeur � comparer
		String value = Tools.getInstance().inputString("\nQuel valeur voulez-vous utiliser ?");
		
		//getLstDocuments selon crit�re ci-dessus
		FindIterable<Document> lstDocuments = this.mongoService.getLstDocuments(collection, lstFields.get(fieldIndex), operatorIndex, value);
		
		for (Document oDoc : lstDocuments) {
			System.out.println(oDoc.toJson());
		}
		
		askWhatToDo(collection);
	}
	
	//Permet d'ajouter un document dans la collection selectionnee
	private void writeInCollection(String collection) {
		//TODO ecriture
		System.out.println("Ecriture");
		
		this.askWhatToDo(collection);
	}
	
	
	
}
