package fr.colyn.tp3.mongo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bson.BSONObject;
import org.bson.Document;
import org.bson.conversions.Bson;

import com.mongodb.MongoClientSettings;
import com.mongodb.ServerAddress;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.MongoIterable;
import com.mongodb.client.model.Filters;

public class MongoService {

	String database_string;

	MongoClient mongoClient;
	MongoDatabase database;

	public MongoService(String host, int port, String database) {
		super();

		this.mongoClient = MongoClients.create(MongoClientSettings.builder()
				.applyToClusterSettings(builder -> builder.hosts(Arrays.asList(new ServerAddress(host, port))))
				.build());

		this.database = mongoClient.getDatabase(database);

	}

	public List<String> getListCollection() {

		List<String> lstStringCollection = new ArrayList<String>();

		MongoIterable<String> collections = this.database.listCollectionNames();

		for (String oCollection : collections) {
			lstStringCollection.add(oCollection);
		}

		return lstStringCollection;
	}

	private MongoCollection<Document> getCollection(String collection) {
		return this.database.getCollection(collection);
	}

	public FindIterable<Document> getLstDocuments(String collection_str, String field, int operatorIndex, String value) {
		MongoCollection<Document> collection = this.database.getCollection(collection_str);
		
		Bson filter = Filters.eq(field, value);
		
		switch (operatorIndex) {
		case 0: //eq
			filter = Filters.eq(field, value);
			break;
		case 1: //ne
			filter = Filters.ne(field, value);
			break;
		case 2: //gt
			filter = Filters.gt(field, value);
			break;
		case 3: //gte
			filter = Filters.gte(field, value);
			break;
		case 4: //lt
			filter = Filters.lt(field, value);
			break;
		case 5: //lte
			filter = Filters.lte(field, value);
			break;

		default:
			break;
		}
		
		return collection.find(filter);
	}

	public List<String> getCollectionFields(String collection_str) {
		
		MongoCollection<Document> collection = this.database.getCollection(collection_str);
		
		FindIterable<Document> itDocument = collection.find();
		
		List<String> keys = new ArrayList<String>();
		
		itDocument.forEach(oDoc -> {
			oDoc.keySet().forEach(oKey -> {
				if (!keys.contains(oKey)) {
					keys.add(oKey);	
				}
			});
		});
		
		return keys;
	}

}
